module.exports = {
  apps: [{
    name: 'dmx',
    script: './run-dmx.js',
    output: './logs/dmx-out.log',
    error: './logs/dmx-error.log',
    restart_delay: 50000,
    env: {
      NODE_ENV: 'production',
      LOGNAME: 'vid:dmx',
      DEBUG: 'vid:dmx:error:*,vid:dmx:warn:*,vid:dmx:log:*',
      DEBUG_HIDE_DATE: 'true',
      DMX_CONFIG: './config-dmx.json'
    }
  }]
};
