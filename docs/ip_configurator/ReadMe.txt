Dieses Archiv enth�lt eine Software.
Diese ist zur Konfiguration der IP-Einstallung
DMX4ALL Artnet Interfaces vorgesehen.

Es besteht keine Gew�hr, dass die enthaltene Software
aktuell und fehlerfrei ist. F�r Sch�den, insbesondere
an Hard- und Software als auch f�r Folgesch�den
�bernehmen wir KEINE Haftung.

----------------------------------------------------------------------

Installation:
- Das Archiv ip-configurator.zip entpacken
- Das Programm IP-Configurator.exe starten

----------------------------------------------------------------------

Kurzanleitung:
- Stellen Sie sicher das nur ein ArtNet-Interface an Ihrem Netzwerk
  verf�gbar ist
- Starten Sie das Programm IP-Configurator.exe
- Nach dem Starten des IP-Configurator wird nach angeschlossenen
  ArtNet-Interfaces gesucht
- Die gefundenen Interfaces werden in der Liste aufgef�hrt
- Klicken Sie auf den Listeneintrag
- Geben Sie die IP und NETMASK ein
- Klicken Sie auf SET
- Ein Warnhinweis erscheint den Sie mit OK best�tigen m�ssen
  um fortzufahren
- Die IP und Netmask wird gesetzt und die Liste aktualisiert

----------------------------------------------------------------------
11.09.2017
