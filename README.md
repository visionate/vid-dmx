
# vid-dmx

## dmx Event / Command

'dmxEvent'

    IDMXEvent {
      deviceID: string | string[];
      data?: DMXCommand | DMXCommand[];
    }

    DMXCommand {
      command: 'color' | 'channels',
      fromColor?: IDMXColor,
      toColor: IDMXColor
      delay?: number;
      fadeTime?: number;
      repeat?: number,
      swing?: boolean
    }

    ColorCommand extends DMXCommand {
      fromColor?: IDMXColor,
      toColor: IDMXColor
    }

    IDMXColor {
      red?: number;
      green?: number;
      blue?: number;
      alpha?: number;
      warm?: number;
      cold?: number;
      dimmer?: number;
      hexColor?: string;
      rgba?: string;
    }



# config

- Lichtschiene an Decke mit DMX Dimmer --> 1 Kanal
- umlaufende Lichtwute mit RGB DMX Steuerung --> RGBCC-LED 5
- Lichtwute hinter den Touchtischen RGB DMX Steuerung --> Schnickschnack 2 Pixel haben RGB-LED 3 Kanäle
- beleuchtung RFID hinter den Touchtischen RGB DMX Steuerung --> RGBCC-LED 5
- beleuchtung RFID unter der Screenwall RGB DMX Steuerung --> RGBCC-LED 5
- beleuchtete Logos an den Stirnseiten --> DMX Dimmer --> 1 Kanal
- Beleuchtung "Modell" hinter einzelnem Touchtisch --> RGBCC-LED 5
- Beleuchtung hinter den iPads--> RGBCC-LED 5

## NPMJS

https://itnext.io/step-by-step-building-and-publishing-an-npm-typescript-package-44fe7164964c

## VID-DMX

DMX-512 controller library for node.js

This configuration file consists of three sections:

- Server
- Universes
- Presets

In the Server section you can set the listen port and host.
Under Universes you describe the DMX Universes with details like which output driver to use and which devices are at which address.
The presets section allows you to specify a state some channels should be set when the preset is called.

### Animation HTTP API

A List of Channel Transistions can be POSTed to <code>/animation/&lt;universe&gt;</code>. Each transistion is a JSON Object with at least the <code>to</code> property present. The Value of which also has to be an Object describing the channel end-states.

A duration for this transistion can be given in the <code>duration</code> property.
If not specified 0ms is assumed.

Example:

	[
		{"to": {"10": 0, "20": 0}},
		{"to": {"10": 255}, "duration": 2000},
		{"to": {"20": 255}, "duration": 1000}
	]

This sets channels 10 and 20 to zero. Then transistions channel 10 to 255 in 2 seconds. After that channel 20 is faded to 255 in 1 second.

#### dmx.registerDriver(name, module)

- <code>name</code> - String
- <code>module</code> - Object implementing the Driver API


Register a new DMX Driver module by its name.
These drivers are currently registered by default:

- null: a development driver that prints the universe to stdout
- artnet: driver for EnttecODE
- bbdmx: driver for [BeagleBone-DMX](https://github.com/boxysean/beaglebone-DMX)
- dmx4all: driver for DMX4ALL devices like the "NanoDMX USB Interface"
- enttec-usb-dmx-pro: a driver for devices using a Enttec USB DMX Pro chip like the "DMXKing ultraDMX Micro".
- enttec-open-usb-dmx: driver for "Enttec Open DMX USB". This device is NOT recommended, there are known hardware limitations and this driver is not very stable. (If possible better obtain a device with the "pro" chip)
- dmxking-utra-dmx-pro: driver for the DMXKing Ultra DMX pro interface. This driver support multiple universe specify the options with Port = A or B

#### dmx.addUniverse(name, driver, device_id, options)

- <code>name</code> - String
- <code>driver</code> - String, referring a registered driver
- <code>device_id</code> - Number or Object
- <code>options</code> - Object, driver specific options

Add a new DMX Universe with a name, driver and an optional device_id used by the driver to identify the device.
For enttec-usb-dmx-pro and enttec-open-usb-dmx device_id is the path the the serial device. For artnet it is the target ip.

#### dmx.update(universe, channels)

- <code>universe</code> - String, name of the universe
- <code>channels</code> - Object, keys are channel numbers, values the values to set that channel to

Update one or multiple channels of a universe. Also emits a <code>update</code> Event with the same information.

#### DMX.devices

A JSON Object describing some Devices and how many channels they use.
Currently not many devices are in there but more can be added to the <code>devices.js</code> file. Pull requests welcome ;-)

The following Devices are known:

- generic - a one channel dimmer
- showtec-multidim2 - 4 channel dimmer with 4A per channel
- eurolite-led-bar - Led bar with 3 RGB color segments and some programms
- stairville-led-par-56 - RGB LED Par Can with some programms

### Class DMX.Animation

#### new DMX.Animation()

Create a new DMX Animation instance. This can be chained similar to jQuery.

#### animation.add(to, duration, options)

- <code>to</code> - Object, keys are channel numbers, values the values to set that channel to
- <code>duration</code> - Number, duration in ms
- <code>options</code> - Object

Add an animation Step.
The options Object takes an <code>easing</code> key which allows to set a easing function from the following list:

- linear (default)
- inQuad
- outQuad
- inOutQuad
- inCubic
- outCubic
- inOutCubic
- inQuart
- outQuart
- inOutQuart
- inQuint
- outQuint
- inOutQuint
- inSine
- outSine
- inOutSine
- inExpo
- outExpo
- inOutExpo
- inCirc
- outCirc
- inOutCirc
- inElastic
- outElastic
- inOutElastic
- inBack
- outBack
- inOutBack
- inBounce
- outBounce
- inOutBounce

Returns a Animation object with the animation step added.

#### animation.delay(duration)

- <code>duration</code> - Number, duration in ms

Delay the next animation step for duration.
Returns a Animation object with the delay step added.

#### animation.run(universe, onFinish)

- <code>universe</code> - Object, reference to the universe driver
- <code>onFinish</code> - Function, called when the animation is done

Run the Animation on the specified universe.

## serial usb on synology

    # check usb
    lsusb

    # install drivers
    sudo insmod /lib/modules/usbserial.ko
    sudo insmod /lib/modules/ftdi_sio.ko

    dmesg | grep tty
        [3868320.479455] usb 2-1.1: FTDI USB Serial Device converter now attached to ttyUSB0

    ls -al /sys/bus/usb-serial/drivers/ftdi_sio/
        ttyUSB0 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.1/2-1.1:1.0/ttyUSB0

## autostart drivers

Edit or create the file startup.sh via:

    sudo vi /usr/local/etc/rc.d/startup.sh
    sudo nano /usr/local/etc/rc.d/startup.sh

Put the next text into the file:

    sudo insmod /lib/modules/usbserial.ko
                                               sudo insmod /lib/modules/ftdi_sio.ko
                                               sudo chmod 777 /lib/modules/usbserial.ko
                                               sudo chmod 777 /lib/modules/ftdi_sio.ko

Change rights of startup.sh:

    sudo chmod 700 /usr/local/etc/rc.d/startup.sh


## mount serial devices DOCKER

    /dev is auto mounted into container when priviliged

    ls -al /dev/tty*

    dmesg | grep tty
    usb 1-1: FTDI USB Serial Device converter now attached to ttyUSB0

    ls -al /sys/bus/usb-serial/drivers/ftdi_sio/

    # devices
    /dev/ttyUSB0 .. 7
