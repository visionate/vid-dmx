export enum IOTypes {
  TCPSocketServer = 'TCPSocketServer',
  TCPSocketClient = 'TCPSocketClient',
  SocketIOServer = 'SocketIOServer',
  KNXConnector = 'KNXConnector',
  OSCServer = 'OSCServer',
  DPDRequest = 'DPDRequest',
}

export enum TargetProtocol {
  KNX = 'KNX',
  TCP = 'TCP',
  IO = 'IO',
  DPD = 'DPD',
  OSC = 'OSC',
}

export class IOObject {
  /**
   * @default ''
   */
  id: string = '';

  /**
   * @default false
   */
  enabled: boolean = false;

  /**
   * @default ''
   */
  IOType: IOTypes | string = '';

  protocol?: TargetProtocol | undefined = undefined;

  targetIDs?: string[] | undefined = undefined;

  targetProtocols?: TargetProtocol[] | undefined = undefined;

  config: any | undefined = undefined;
}
