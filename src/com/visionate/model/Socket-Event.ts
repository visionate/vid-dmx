import { TargetProtocol } from './IO-Object';

export class SocketEvent {
  event: string = '';

  senderAddress?: string | undefined = undefined;

  destination?: string | undefined = undefined;

  // get | post | put | delete
  method?: 'get' | 'post' | 'put' | 'delete' | string | undefined = undefined;

  targetProtocols?: TargetProtocol[] | undefined = undefined;

  value: { type?: string; data?: any } | undefined = undefined;
}
