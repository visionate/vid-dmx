import debug0 from 'debug';

if ( !process.env.LOGNAME ) {
  process.env.LOGNAME = 'vid:dmx';
}
let debug = debug0( process.env.LOGNAME );

let error = debug.extend( 'error' );

let warn = debug.extend( 'warn' );
warn.log = console.warn.bind( console );

let log = debug.extend( 'log' );
log.log = console.info.bind( console );

let trace = debug.extend( 'trace' );
trace.log = console.debug.bind( console );

export class CLog {
  public static createClogger<TA>( name: string ): CLogger {
    return new CLogger( name );
  }
}

export class CLogger {
  private logger;

  private warner;

  private tracer;

  private errorLogger;

  constructor( name: string ) {
    this.errorLogger = error.extend( name );
    this.logger = log.extend( name );
    this.warner = warn.extend( name );
    this.tracer = trace.extend( name );
  }

  public trace( ...otherParams: any[] ): void {
    // @ts-ignore
    this.tracer( ...otherParams );
  }

  public log( ...otherParams: any[] ): void {
    // @ts-ignore
    this.logger( ...otherParams );
  }

  public info( ...otherParams: any[] ): void {
    // @ts-ignore
    this.logger( ...otherParams );
  }

  public warn( ...otherParams: any[] ): void {
    // @ts-ignore
    this.warner( ...otherParams );
  }

  public error( ...otherParams: any[] ): void {
    // @ts-ignore
    this.errorLogger( ...otherParams );
  }
}
