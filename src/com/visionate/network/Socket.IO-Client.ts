import * as events from 'events';
import IO from 'socket.io-client';
import IOWC from 'socketio-wildcard';
import * as URL from 'url';
import { CLog, CLogger } from '../logging/Logger';
import { SocketEvent } from '../model/Socket-Event';
import { escapeJSON } from '../utils/Utils';
import Socket = SocketIOClient.Socket;

export interface IIOClient extends events.EventEmitter {
  addListener( event: 'dmxEvent', listener: ( clientEvent: SocketEvent ) => void ): this;
  emit( event: '*', wildcardEvent: Event ): boolean;
  on( event: '*', listener: ( wildcardEvent: Event ) => void ): this;
}

export class IOClient extends events.EventEmitter implements IIOClient {
  public logger: CLogger = CLog.createClogger( 'SocketIOClient' );

  public client!: Socket;

  private port!: number;

  public proto!: string;

  public host!: string;

  public listenEvents: string[] = [];

  constructor() {
    super();
  }

  public createClient( proto: string, host: string, port: number, listenEvents: string[] ) {
    this.logger.info( 'SocketClient createClient:', proto, port, host );

    this.proto = proto;
    this.port = port;
    this.host = host;
    this.listenEvents = listenEvents;

    const url = URL.format( {
      protocol: this.proto,
      hostname: this.host,
      port: this.port,
      slashes: true,
    } );

    this.client = IO( url, { autoConnect: false } );
    // piggyback using the event-emitter bundled with socket.io client
    const patch = IOWC( IO.Manager );
    patch( this.client );

    this.client.on( 'connect', () => {
      this.logger.info( 'socket on connect' );
    } );

    this.client.on( 'disconnect', () => {
      this.logger.info( 'socket on disconnect' );
      setTimeout( () => {
        this.connect();
      }, 5000 );
    } );

    this.client.on( 'error', ( error: any ) => {
      this.logger.error( 'socket on error: %o', error );
      setTimeout( () => {
        this.connect();
      }, 5000 );
    } );

    this.client.on( '*', ( event: any ) => {

      let eventName: string | undefined;

      try {
        eventName = event.data[ 0 ];
      } catch ( e ) {
        this.logger.error( 'on * error: %O', e );
      }
      if ( eventName && this.listenEvents.indexOf( eventName ) > -1 ) {
        let data: any | undefined = event.data[ 1 ];
        try {
          if ( data && typeof data === 'string' ) {
            data = JSON.parse( escapeJSON( data ) );
          }
        } catch ( e ) {
          this.logger.error( 'on * aka (%s) Data JSON Parse error:%O', e );
        }
        this.logger.trace( 'on * aka (%s) Data : [%o]', eventName, data );
        this.emit( eventName, data );
      }
    } );

    return this;
  }

  public connect() {
    this.logger.info( 'SocketClient connect' );
    if ( this.client.connected === false ) {
      this.client.connect();
    }
  }

  public disconnect() {
    this.logger.info( 'SocketClient disconnect' );
    this.client.disconnect();
  }

  public send( event: string, data: any ) {
    let dataString: string = '';
    if ( typeof data === 'string' ) {
      dataString = data;
    } else {
      dataString = JSON.stringify( data );
    }
    this.client.emit( event, dataString );
  }
}
