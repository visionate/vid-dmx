import * as events from 'events';
import { throttle } from 'lodash';
import { CLog, CLogger } from '../../visionate/logging/Logger';
import { IChannels, IDriver } from '../DMX-Controller';

export class TestDevice implements IDriver {
  public static DRIVER_NAME: string = 'TEST';

  static UNIVERSE_LEN = 512;

  public universe: Buffer;

  public currentChannels: IChannels = {};

  public dev: events.EventEmitter;

  public logger: CLogger = CLog.createClogger( 'DMX4allUSB' );

  protected sendUniverseThrottled: Function;

  protected debugUniverseThrottled: Function;

  constructor( deviceId: string, options: any ) {

    this.dev = new events.EventEmitter();
    this.dev.emit( 'open' );

    // keep alive
    setInterval( () => {
      this.logger.info( 'ping' );
    }, 100000 );

    this.universe = Buffer.alloc( TestDevice.UNIVERSE_LEN + 1, 0 );

    this.sendUniverseThrottled = throttle( this.sendUniverse, 25 ); // send with 40 Hz
    this.debugUniverseThrottled = throttle( this.debugUniverse, 1000 ); // every second
  }

  protected sendUniverse() {
    this.debugUniverseThrottled();
  }

  protected debugUniverse() {
    this.logger.trace( 'sendUniverse %o', this.currentChannels );
  }

  public close( cb: any ) {
    this.dev.emit( 'close' );
    if ( cb ) {
      cb.call();
    }
  }

  public update( channels: IChannels ) {
    for ( const channelIndex in channels ) {
      if ( channels.hasOwnProperty( channelIndex ) ) {
        this.currentChannels[ channelIndex ] = channels[ channelIndex ];
        this.universe[ parseInt( channelIndex ) ] = channels[ channelIndex ];
      }
    }
    this.sendUniverseThrottled();
  }

  public updateAll( value: number, universeIndex?: number ) {
    for ( let i = 1; i <= TestDevice.UNIVERSE_LEN; i++ ) {
      this.universe[ i ] = value;
    }
    this.sendUniverseThrottled( universeIndex );
  }

  public get( channelIndex: string | number ): number {
    return this.universe[ channelIndex as number ];
  }
}
