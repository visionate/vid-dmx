// TODO: https://github.com/margau/dmxnet
// TODO: https://github.com/mtraeger/artnet-weblight

// @ts-ignore
import artnet from 'artnet';
import * as events from 'events';
import { get, throttle } from 'lodash';
import { CLog, CLogger } from '../../visionate/logging/Logger';
import { IChannels, IDriver } from '../DMX-Controller';

export interface IArtnet {
  set( universe?: number, channel?: number, values?: number | number[], callback?: ( err: any, res: any ) => {} ): void;
  set( channel: number, value: number, callback?: ( err: any, res: any ) => {} ): void;
  close(): void;
}

interface IArtnetOptions {
  subnet: number;
  universe: number;
  net: number;
}

interface IArtnetConfig {
  host?: string; // '255.255.255.255';
  port?: number; // 6454;
  refresh?: number; // 4000;
  sendAll?: boolean; // false;
  subnet?: number; // 0
  universe?: number; // 0
  net?: number; // 0
  universeLen?: number; // 512
  numUniverses?: number; // 1-16
}

export class ArtNet implements IDriver {

  public static DRIVER_NAME: string = 'ArtNet';

  public logger: CLogger = CLog.createClogger( 'ArtNet' );

  public universe: number[];

  public currentChannels: IChannels = {};

  private artnetDev: IArtnet;

  public dev: events.EventEmitter;

  protected sendUniverseThrottled: Function;

  protected debugUniverseThrottled: Function;

  protected config: IArtnetConfig;

  protected options: IArtnetOptions;

  constructor( deviceId: string, _config: IArtnetConfig ) {

    this.logger.log( 'constructor %s options:%O', deviceId, _config );

    this.config = {
      host: deviceId || '255.255.255.255',
      port: _config.port || 6454,
      refresh: _config.refresh || 4000,
      sendAll: _config.sendAll || true,
      subnet: _config.subnet || 0,
      universe: _config.universe || 0,
      net: _config.net || 0,
      universeLen: _config.universeLen || 512,
      numUniverses: _config.numUniverses || 4,
    };

    this.options = {
      subnet: this.config.subnet!,
      universe: this.config.universe!,
      net: this.config.net!,
    };

    this.dev = new events.EventEmitter();

    this.universe = new Array( (this.config.universeLen! * this.config.numUniverses!) ).fill( 0 );

    this.logger.info( 'starting dmxnet ip, port', this.config.host, this.config.port );

    this.artnetDev = artnet( this.config );
    (this.artnetDev as any).on( 'error', ( error: any ) => {
      this.dev.emit( 'error', error );
    } );

    this.sendUniverseThrottled = throttle( this.sendUniverse, 25 ); // send with 40 Hz
    this.debugUniverseThrottled = throttle( this.debugUniverse, 1000 ); // every second
    setTimeout( () => {
      this.dev.emit( 'open', this.config );
    }, 2000 );
  }

  protected debugUniverse( universeIndex?: number ) {
    this.logger.trace( 'sendUniverse universe universeIndex: %s, %o', universeIndex, this.currentChannels );
  }

  protected sendUniverse( universeIndex?: number ) {
    this.debugUniverseThrottled( universeIndex );
    try {
      const startUniverse = typeof universeIndex !== 'undefined' ? universeIndex : this.options.universe;
      let i = 0;
      while ( i < this.config.numUniverses! ) {
        const startIndex = (i * this.config.universeLen!);
        const endIndex = (startIndex + this.config.universeLen!);
        const univerData: number[] = this.universe.slice( startIndex, endIndex );
        const currentUniverseIndex = i + startUniverse;
        this.logger.trace(
          'sendUniverse universe universeIndex: currentUniverseIndex:%s, startIndex:%s endIndex:%s, univerData.length:%s',
          currentUniverseIndex, startIndex, endIndex, univerData.length );
        this.artnetDev.set( currentUniverseIndex, 1, univerData );
        i++;
      }

    } catch ( e ) {
      this.dev.emit( 'error', e );
    }
  }

  public close( cb: any ) {
    this.logger.info( 'artnet close' );
    try {
      this.artnetDev.close();
      this.dev.emit( 'close' );
    } catch ( e ) {
      this.dev.emit( 'error', e );
    }
    if ( cb ) {
      cb( null );
    }
  }

  public update( channels: IChannels, universeIndex?: number ) {
    this.logger.trace( 'sendUniverse update universeIndex: %s', universeIndex );
    for ( const channelIndex in channels ) {
      if ( channels.hasOwnProperty( channelIndex ) ) {
        this.currentChannels[ channelIndex ] = channels[ channelIndex ];
        this.universe[ parseInt( channelIndex ) - 1 ] = channels[ channelIndex ];
      }
    }
    this.sendUniverseThrottled( universeIndex );
  }

  public updateAll( value: number, universeIndex?: number ) {
    for ( let i = 0; i <= (this.config.universeLen! * this.config.numUniverses!); i++ ) {
      this.universe[ i ] = value;
    }
    this.sendUniverseThrottled( universeIndex );
  }

  public get( channelIndex: string | number ): number {

    return get( this.universe, channelIndex );
  }
}
