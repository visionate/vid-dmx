import * as events from 'events';
import { throttle } from 'lodash';
import SerialPort from 'serialport';
import { CLog, CLogger } from '../../visionate/logging/Logger';
import { IChannels, IDriver } from '../DMX-Controller';

export class DMX4allUSB implements IDriver {

  public static DRIVER_NAME: string = 'DMX4allUSB';

  static UNIVERSE_LEN = 512;

  public universe: Buffer;

  public currentChannels: IChannels = {};

  public dev: events.EventEmitter;

  private devSerialPort!: SerialPort;

  public logger: CLogger = CLog.createClogger( 'DMX4allUSB' );

  protected sendUniverseThrottled: Function;

  protected debugUniverseThrottled: Function;

  constructor( deviceId: string, options: any ) {

    this.dev = new events.EventEmitter();

    this.universe = Buffer.alloc( DMX4allUSB.UNIVERSE_LEN + 1, 0 );

    this.sendUniverseThrottled = throttle( this.sendUniverse, 25 ); // send with 40 Hz
    this.debugUniverseThrottled = throttle( this.debugUniverse, 1000 ); // every second

    setTimeout( () => {
      this.devSerialPort = new SerialPort( deviceId, options );

      this.devSerialPort.on( 'error', error => {
        this.logger.error( 'Serialport on Error %O', error );
        this.dev.emit( 'error', error );
      } );
      this.devSerialPort.on( 'open', () => {
        this.logger.info( 'DMX Serialport is open!' );
        this.dev.emit( 'open', options );
      } );

      this.devSerialPort.on( 'close', () => {
        this.logger.info( 'DMX Serialport is closed!' );
        this.dev.emit( 'close', options );
      } );

      /*this.devSerialPort.on( 'data', data => {
       // process.stdout.write(data.toString('ascii'));
       } );*/
    }, 2000 );
  }

  protected sendUniverse() {
    this.debugUniverseThrottled();
    if ( this.devSerialPort && this.devSerialPort.isOpen ) {
      const msg = Buffer.alloc( DMX4allUSB.UNIVERSE_LEN * 3 );

      for ( let i = 0; i < DMX4allUSB.UNIVERSE_LEN; i++ ) {
        msg[ i * 3 ] = i < 256 ? 0xe2 : 0xe3;
        msg[ i * 3 + 1 ] = i;
        msg[ i * 3 + 2 ] = this.universe[ i + 1 ];
      }

      this.writeAndDrain( msg, ( error?: Error | null ) => {
        if ( error ) {
          this.logger.error( 'Error on write: %o', error );
          //  this.emit( 'error', error );
        }
      } );
    }
  }

  protected debugUniverse() {
    this.logger.trace( 'sendUniverse %o', this.currentChannels );
  }

  protected writeAndDrain( data: any, callback: SerialPort.ErrorCallback ) {
    if ( this.devSerialPort && this.devSerialPort.isOpen ) {
      callback( null );
      if ( this.devSerialPort.write( data ) ) {
      } else {
        this.devSerialPort.drain( callback );
      }
    } else {
      callback( new Error( 'write => Port is not open!' ) );
    }
  }

  public close( cb: any ) {
    this.devSerialPort.close( cb );
    this.dev.emit( 'close' );
  }

  public update( channels: IChannels ) {
    for ( const channelIndex in channels ) {
      if ( channels.hasOwnProperty( channelIndex ) ) {
        this.currentChannels[ channelIndex ] = channels[ channelIndex ];
        this.universe[ parseInt( channelIndex ) ] = channels[ channelIndex ];
      }
    }
    this.sendUniverseThrottled();
  }

  public updateAll( value: number, universeIndex?: number ) {
    for ( let i = 1; i <= DMX4allUSB.UNIVERSE_LEN; i++ ) {
      this.universe[ i ] = value;
    }
    this.sendUniverseThrottled( universeIndex );
  }

  public get( channelIndex: string | number ): number {
    return this.universe[ channelIndex as number ];
  }
}
