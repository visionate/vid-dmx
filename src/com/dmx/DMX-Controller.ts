﻿import * as events from 'events';
import { set } from 'lodash';
import { CLog, CLogger } from '../visionate/logging/Logger';
import { ArtNet } from './drivers/artnet';
import { DMX4allUSB } from './drivers/DMX4all-USB';
import { TestDevice } from './drivers/Test-Device';

export interface IDMXController extends events.EventEmitter {
  emit( event: 'update', universe: string, channels: IChannels ): boolean;
  on( event: 'update', listener: ( universe: string, channels: IChannels ) => void ): this;
  emit( event: 'updateAll', universe: string, value: number ): boolean;
  on( event: 'updateAll', listener: ( universe: string, value: number ) => void ): this;
}

export type DriverClass = new ( deviceId: string, options: any ) => IDriver;

export interface DriverClasss {
  [ key: string ]: DriverClass;
}

export interface IUniversese {
  [ key: string ]: IDriver;
}

export interface IChannels {
  [ key: string ]: number;
}

export interface IChannelGroups {
  [ key: string ]: string[];
}

export interface IDriver {
  dev: events.EventEmitter;
  close( callBack?: Function ): void;
  update( channels: IChannels, universeIndex?: number ): void;
  updateAll( value: number, universeIndex?: number ): void;
  get( channelIndex: string | number ): void;
}

export class DMXController extends events.EventEmitter implements IDMXController {
  public logger: CLogger = CLog.createClogger( 'DMXController' );

  public universes: IUniversese = {};

  public drivers: DriverClasss = {};

  constructor() {
    super();
    this.registerDriver( DMX4allUSB.DRIVER_NAME, DMX4allUSB );
    this.registerDriver( ArtNet.DRIVER_NAME, ArtNet );
    this.registerDriver( TestDevice.DRIVER_NAME, TestDevice );
  }

  public registerDriver( name: string, module: DriverClass ) {
    this.drivers[ name ] = module;
  }

  public addUniverse( name: string, driver: string, deviceId: string, options?: any ) {
    this.universes[ name ] = new this.drivers[ driver ]( deviceId, options );
    return this.universes[ name ];
  }

  public update( universe: string, channels: IChannels, universeIndex?: number ) {
    this.universes[ universe ].update( channels, universeIndex );
    this.emit( 'update', universe, channels );
  }

  public updateAll( universe: string, value: number, universeIndex?: number ) {
    this.universes[ universe ].updateAll( value, universeIndex );
    this.emit( 'updateAll', universe, value );
  }

  public universeToObject( universeKey: string ): IChannels {
    const universe = this.universes[ universeKey ];
    const u = {};
    for ( let i = 0; i < 512; i++ ) {
      set( u, i, universe.get( i ) );
    }
    return u;
  }

  public disconnect(): Promise<void> {
    this.universes[ 'DMX' ].close();
    return Promise.resolve();
  }
}
