import { temp2rgb } from '../../../color-temp';

export interface IDMXColor {
  red?: number;
  green?: number;
  blue?: number;
  alpha?: number;
  warm?: number;
  cold?: number;
  dimmer?: number;
  hexColor?: string;
  rgba?: string;
  celvin?: number;
}

export class DMXColor implements IDMXColor {
  constructor( color: IDMXColor ) {

    this.dimmer = typeof color.dimmer !== 'undefined' ? color.dimmer : 255;
    this.alpha = typeof color.alpha !== 'undefined' ? color.alpha : 1;

    this._red = typeof color.red !== 'undefined' ? color.red : 0;
    this._green = typeof color.green !== 'undefined' ? color.green : 0;
    this._blue = typeof color.blue !== 'undefined' ? color.blue : 0;

    if ( color.hexColor && color.hexColor !== '' ) {
      this.hexColor = color.hexColor;
    }
    if ( color.celvin && color.celvin > -1 ) {
      this.celvin = color.celvin;
    }
    if ( color.rgba && color.rgba !== '' ) {
      this.rgba = color.rgba;
    }

    this._warm = typeof color.warm !== 'undefined' ? color.warm : 0;
    this._cold = typeof color.cold !== 'undefined' ? color.cold : 0;
  }

  // 0-255
  public dimmer?: number = 255;

  // 0-1
  public alpha: number = 1;

  private _red: number = 0;

  private _green: number = 0;

  private _blue: number = 0;

  private _warm: number = 0;

  private _cold: number = 0;

  private _celvin: number = 0;

  public get red(): number {
    return this._red * this.alpha;
  }

  public set red( value: number ) {
    this._red = value;
  }

  public get green(): number {
    return this._green * this.alpha;
  }

  public set green( value: number ) {
    this._green = value;
  }

  public get blue(): number {
    return this._blue * this.alpha;
  }

  public set blue( value: number ) {
    this._blue = value;
  }

  public get hexColor(): string {
    const componentToHex = ( c: any ) => {
      const hex = c.toString( 16 );
      return hex.length == 1 ? '0' + hex : hex;
    };
    return '#' + componentToHex( this.red ) + componentToHex( this.green ) + componentToHex( this.blue );
  }

  public set hexColor( value: string ) {
    value = value.replace( '#', '' );
    if ( value.length === 6 ) {
      this.red = parseInt( value.substring( 0, 2 ), 16 );
      this.green = parseInt( value.substring( 2, 4 ), 16 );
      this.blue = parseInt( value.substring( 4, 6 ), 16 );
    }
  }

  public get celvin(): number {
    return this._celvin;
  }

  public set celvin( value: number ) {

    const rgbCelvin = temp2rgb( value );
    console.log( rgbCelvin ); // ~[ 255, 67, 0 ]

    this.red = parseInt( rgbCelvin[ 0 ] as any );
    this.green = parseInt( rgbCelvin[ 1 ] as any );
    this.blue = parseInt( rgbCelvin[ 2 ] as any );
  }

  // value:'255,255,255,0.5'
  public set rgba( value: string ) {
    const rgba: string[] = value.split( ',' );
    if ( rgba && rgba.length === 4 ) {
      this.red = parseInt( rgba[ 0 ] );
      this.green = parseInt( rgba[ 1 ] );
      this.blue = parseInt( rgba[ 2 ] );
      this.alpha = parseFloat( rgba[ 3 ] );
    } else {
      console.warn( 'wrong rgba length', rgba );
    }
  }

  public get rgba(): string {
    return '' + this.red + ',' + this.green + ',' + this.blue + ',' + this.alpha;
  }

  public get warm(): number {
    return this._warm;
  }

  public set warm( value: number ) {
    this._warm = value;
  }

  public get cold(): number {
    return this._cold;
  }

  public set cold( value: number ) {
    this._cold = value;
  }
}
