import { IDMXColor } from './DMXColor';

export type DMXCommandName = 'color';

export interface IDMXEvent {
  deviceID: string | string[];
  data?: DMXCommand | DMXCommand[];
}

export interface DMXCommand {
  command: DMXCommandName | string;
  delay?: number;
  fadeTime?: number;
  repeat?: number,
  swing?: boolean
  resetTweens?: boolean,
  universeIndex?: number
}

export interface ColorCommand extends DMXCommand {
  fromColor?: IDMXColor,
  toColor: IDMXColor
}

export class DMXEvent implements IDMXEvent {
  constructor( event: IDMXEvent ) {
    if ( !event.deviceID ) {
      //
    } else {
      this.deviceID = event.deviceID;
      this.data = event.data!;
    }
  }

  public deviceID!: string | string[];

  public data!: DMXCommand | DMXCommand[];
}
