﻿import { cloneDeep, isArray, mapValues, round, set } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { autoPlay, remove, Tween, update } from '../../es6-tweens';
import { DeviceConfig } from '../../vid-dmx';
import { CLog, CLogger } from '../visionate/logging/Logger';
import { IChannelGroups, IChannels, IDriver } from './DMX-Controller';
import { DMXColor } from './model/DMXColor';

/*var autoPlay: any, remove: any, update: any = require( 'es6-tween' );
 var es6Tween = require( 'es6-tween' );*/

type ColorIndex = 'r' | 'g' | 'b' | 'c' | 'w' | 'd';
type RGBCWD = { [k in ColorIndex]?: number };

export class DMXDevice {
  public logger: CLogger;

  public deviceID: string;

  public universe: IDriver;

  private channelsConfig: string[];

  private readonly startChannel: number;

  private readonly numChannels: number;

  private readonly numGroups: number;

  private allChannels: number[] = [];

  private rgbcwdChannels: RGBCWD[] = [];

  // channels with actual Data
  private currentChannels: IChannels = {};

  public get deviceChannels$(): Observable<IChannels> {
    return this.$deviceChannels.asObservable();
  };

  private $deviceChannels: BehaviorSubject<IChannels> = new BehaviorSubject<IChannels>( {} );

  // groups with actual Data
  private groups: IChannelGroups = {};

  private readonly hasR: boolean = false;

  private readonly rChannelIndex: number;

  private readonly hasG: boolean = false;

  private readonly gChannelIndex: number;

  private readonly hasB: boolean = false;

  private readonly bChannelIndex: number;

  // coldhite
  private readonly hasC: boolean = false;

  private readonly cChannelIndex: number;

  // warmWhite
  private readonly hasW: boolean = false;

  private readonly wChannelIndex: number;

  // dimmer
  private readonly hasD: boolean = false;

  private readonly dChannelIndex: number;

  // es6-tween
  public tweens: Tween[] = [];

  public currentTweenTimeouts: any[] = [];

  constructor( id: string, config: DeviceConfig, universe: IDriver ) {
    this.logger = CLog.createClogger( 'DMXDevice [' + id + ']' );

    // this.logger.info( 'config', config );
    // this.logger.info( 'type', type );

    this.deviceID = id;

    const groupsAreContinues: boolean = !isArray( config.groups );

    this.startChannel = config.address;

    if ( groupsAreContinues ) {
      this.numGroups = config.groups as number;
    } else {
      this.numGroups = (config.groups as number[]).length;
    }

    this.channelsConfig = config.channels;
    this.numChannels = this.channelsConfig.length * this.numGroups;

    this.universe = universe;

    this.rChannelIndex = this.channelsConfig.indexOf( 'r' );
    this.hasR = this.channelsConfig.indexOf( 'r' ) > -1;
    this.gChannelIndex = this.channelsConfig.indexOf( 'g' );
    this.hasG = this.channelsConfig.indexOf( 'g' ) > -1;
    this.bChannelIndex = this.channelsConfig.indexOf( 'b' );
    this.hasB = this.channelsConfig.indexOf( 'b' ) > -1;

    this.wChannelIndex = this.channelsConfig.indexOf( 'w' );
    this.hasW = this.channelsConfig.indexOf( 'w' ) > -1;

    this.cChannelIndex = this.channelsConfig.indexOf( 'c' );
    this.hasC = this.channelsConfig.indexOf( 'c' ) > -1;

    this.dChannelIndex = this.channelsConfig.indexOf( 'd' );
    this.hasD = this.channelsConfig.indexOf( 'd' ) > -1;

    // this.logger.info( 'cChannelIndex %d hasC: %s ', cChannelIndex, this.hasC );

    const numGroupChannels = this.channelsConfig.length;

    if ( groupsAreContinues ) {
      let channelIndex = this.startChannel;
      for ( let groupNumber = 1; groupNumber < this.numGroups + 1; groupNumber++ ) {

        this.groups[ groupNumber ] = [];
        const rgbcwChannels: RGBCWD = {};
        for ( let channelNumber = 1; channelNumber < numGroupChannels + 1; channelNumber++ ) {
          this.allChannels.push( channelIndex );
          if ( this.rChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'r' ] = channelIndex;
          }
          if ( this.gChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'g' ] = channelIndex;
          }
          if ( this.bChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'b' ] = channelIndex;
          }
          if ( this.wChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'w' ] = channelIndex;
          }
          if ( this.cChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'c' ] = channelIndex;
          }
          if ( this.dChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'd' ] = channelIndex;
          }

          this.groups[ groupNumber ].push( channelIndex.toString() );
          this.currentChannels[ channelIndex ] = 0;
          channelIndex++;
        }

        this.rgbcwdChannels.push( rgbcwChannels );
      }
    } else {

      (config.groups as number[]).forEach( groupChannel => {
        this.logger.trace( 'groupChannel %O', groupChannel );
        this.groups[ groupChannel ] = [];
        const rgbcwChannels: RGBCWD = {};
        let channelIndex = groupChannel === 1 ? groupChannel : ((groupChannel - 1) * numGroupChannels) + 1;
        for ( let channelNumber = 1; channelNumber < numGroupChannels + 1; channelNumber++ ) {
          this.allChannels.push( channelIndex );
          if ( this.rChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'r' ] = channelIndex;
          }
          if ( this.gChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'g' ] = channelIndex;
          }
          if ( this.bChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'b' ] = channelIndex;
          }
          if ( this.wChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'w' ] = channelIndex;
          }
          if ( this.cChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'c' ] = channelIndex;
          }
          if ( this.dChannelIndex + 1 === channelNumber ) {
            rgbcwChannels[ 'd' ] = channelIndex;
          }

          this.groups[ groupChannel ].push( channelIndex.toString() );
          this.currentChannels[ channelIndex ] = 0;
          channelIndex++;
        }

        this.rgbcwdChannels.push( rgbcwChannels );
      } );

    }

    // initial update
    // this.universe.update( this.currentChannels );
    // this.$deviceChannels.next( this.currentChannels );

    this.logger.trace( 'rgbcwChannels %O', this.rgbcwdChannels );
    this.logger.trace( 'allChannels %o', this.allChannels );
    this.logger.trace( 'currentChannels %o', this.currentChannels );

  }

  /**
   *
   * @param fromColor
   * @param toColor
   * @param delay
   * @param fadeTime milliseconds
   * @param repeat
   * @param swing
   * @param resetTweens
   * @param universeIndex
   */
  public setColor( fromColor: DMXColor | null, toColor: DMXColor, delay?: number, fadeTime?: number, repeat?: number,
    swing?: boolean, resetTweens?: boolean, universeIndex?: number ) {

    autoPlay( true );

    if ( typeof resetTweens === 'undefined' || resetTweens === null ) {
      resetTweens = true;
    }

    if ( resetTweens && this.tweens && this.tweens.length > 0 ) {
      while ( this.tweens.length > 0 ) {
        let t: Tween = this.tweens.splice( 0, 1 )[ 0 ];
        if ( t && t.isPlaying() ) {
          t.stop();
          this.logger.trace( 'tween stopped id: %s', t.id );
        }
        (t as any).off( 'start' );
        (t as any).off( 'update' );
        (t as any).off( 'stop' );
        (t as any).off( 'complete' );
        remove( t );
        // t = void 0;
      }
      this.tweens = [];

      while ( this.currentTweenTimeouts.length > 0 ) {
        let to = this.currentTweenTimeouts.splice( 0, 1 )[ 0 ];
        clearTimeout( to );
        to = void 0;
        this.logger.trace( 'timeout removed' );
      }
      this.currentTweenTimeouts = [];
    }

    setTimeout( () => {

      if ( typeof delay === 'undefined' || isNaN( delay ) ) {
        delay = 0;
      }

      if ( typeof fadeTime === 'undefined' || isNaN( fadeTime ) ) {
        fadeTime = 0;
      }

      if ( typeof fromColor !== 'undefined' && fromColor !== null ) {

        for ( const channelIndex in this.rgbcwdChannels ) {
          const rgbIndex: RGBCWD = this.rgbcwdChannels[ channelIndex ];
          if ( this.hasR ) {
            this.currentChannels[ rgbIndex.r! ] = fromColor.red;
          }
          if ( this.hasG ) {
            this.currentChannels[ rgbIndex.g! ] = fromColor.green;
          }
          if ( this.hasB ) {
            this.currentChannels[ rgbIndex.b! ] = fromColor.blue;
          }
          if ( this.hasC ) {
            this.currentChannels[ rgbIndex.c! ] = fromColor.cold;
          }
          if ( this.hasW ) {
            this.currentChannels[ rgbIndex.w! ] = fromColor.warm;
          }
          if ( this.hasD ) {
            this.currentChannels[ rgbIndex.d! ] = fromColor.dimmer!;
          }
        }
      }

      const toChannels = {};
      for ( const channelIndex in this.rgbcwdChannels ) {
        const rgbIndex: RGBCWD = this.rgbcwdChannels[ channelIndex ];
        if ( this.hasR ) {
          set( toChannels, rgbIndex.r!, toColor.red );
        }
        if ( this.hasG ) {
          set( toChannels, rgbIndex.g!, toColor.green );
        }
        if ( this.hasB ) {
          set( toChannels, rgbIndex.b!, toColor.blue );
        }
        if ( this.hasC ) {
          set( toChannels, rgbIndex.c!, toColor.cold );
        }
        if ( this.hasW ) {
          set( toChannels, rgbIndex.w!, toColor.warm );
        }
        if ( this.hasD ) {
          set( toChannels, rgbIndex.d!, toColor.dimmer );
        }
      }

      const createTween = () => {
        this.logger.trace( 'createTween' );

        // init tween
        let tween: Tween;
        tween = new Tween( cloneDeep( this.currentChannels ) );
        // tween.setMaxListener( 50 );

        // this.logger.info( 'setColor %o currentChannels %o', toColor, this.currentChannels );

        this.logger.trace( 'tween.to', toChannels, fadeTime );
        tween.to( toChannels, fadeTime );

        tween.on( 'start', ( currentChannels: any ) => {
          this.logger.trace( 'tween on start %s', tween.id );
        } );

        tween.on( 'stop', ( currentChannels: any ) => {
          this.logger.trace( 'tween on stop %s', tween.id );
        } );

        tween.on( 'update', ( currentChannels: any, elapsed: any, time: any ) => {

          // this.logger.trace( 'tween on update %s', tween.id );

          const rounded = cloneDeep( mapValues( currentChannels, ( val ) => {
            return round( val, 0 );
          } ) );

          // this.logger.trace( 'tween on update %O %s %s', tween, elapsed, time );

          this.currentChannels = rounded;
          this.$deviceChannels.next( rounded );
          this.logger.trace( 'update universeIndex %s', universeIndex );
          this.universe.update( rounded, universeIndex );

        } );

        tween.on( 'complete', ( currentChannels: any ) => {
          this.logger.trace( 'tween on complete %s', tween.id );
          (tween as any).off( 'start' );
          (tween as any).off( 'update' );
          (tween as any).off( 'stop' );
          (tween as any).off( 'complete' );
        } );

        if ( typeof repeat !== 'undefined' && !isNaN( repeat ) && repeat !== 0 ) {

          if ( repeat === -1 ) {
            repeat = Infinity;
          }
          this.logger.trace( 'tween.repeat', repeat );
          tween.repeat( repeat );
        }

        if ( typeof swing !== 'undefined' && swing ) {
          this.logger.trace( 'tween.yoyo', swing );
          tween.yoyo( swing );
        }

        this.tweens.push( tween );

        this.logger.trace( 'tween start tween: %s', tween.id );

        tween.start();

        setTimeout( () => {
          this.logger.trace( 'tween start update' );
          // hack weil der tween beim 2ten aufruf nicht losläuft
          update();
        }, 28 );
      };

      if ( delay > 0 && (fadeTime && fadeTime > 0) ) {
        this.currentTweenTimeouts.push( setTimeout( createTween, delay ) );
      } else if ( delay === 0 && (fadeTime && fadeTime > 0) ) {
        createTween();
      } else {
        this.currentChannels = toChannels;
        this.$deviceChannels.next( toChannels );
        this.logger.trace( 'no tween set %s to %o', universeIndex, toChannels );
        this.universe.update( toChannels, universeIndex );
      }
    }, 28 );
  }
}
