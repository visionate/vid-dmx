import {
  add,
  autoPlay,
  FrameThrottle,
  get,
  getAll,
  has,
  isRunning,
  now,
  onRequestTick,
  onTick,
  Plugins,
  remove,
  removeAll,
  ToggleLagSmoothing,
  update,
} from './core';
import Easing from './Easing';
import Interpolation from './Interpolation';
import Tween from './Tween';
import Timeline from './Timeline';
import Selector from './selector';
import Interpolator from './Interpolator';
import * as utils from './constants';
import './shim';

export {
  Plugins,
  Selector,
  Interpolator,
  onTick,
  has,
  get,
  getAll,
  removeAll,
  remove,
  add,
  now,
  update,
  autoPlay,
  isRunning,
  onRequestTick,
  FrameThrottle,
  ToggleLagSmoothing,
  Tween,
  Timeline,
  Easing,
  Interpolation,
  utils,
};
