import { join } from 'path';
import { CLog, CLogger } from './com/visionate/logging/Logger';
import { DMXManager } from './vid-dmx';

const logger: CLogger = CLog.createClogger( 'dmx' );

let dmxManager: DMXManager;

process.on( 'uncaughtException', err => {
  logger.error( 'uncaughtException:%s %O', err.message, err );
} );

process.on( 'SIGINT' || 'SIGTERM', () => {
  logger.info( 'Got SIGINT | SIGTERM. Graceful shutdown pid: %s', process.pid );
  if ( dmxManager ) {
    dmxManager
      .shutdown()
      .then( () => process.exit( 0 ) )
      .catch( e => process.exit( 1 ) );
  } else {
    process.exit( 0 );
  }
} );

function initDMX( config: any ) {
  logger.trace( 'initDMX config: %O', config );
  dmxManager = new DMXManager( config );
}

if ( process.env.DMX_CONFIG ) {
  logger.trace( 'process.env.DMX_CONFIG: %o', process.env.DMX_CONFIG );
  const configPath = process.env.DMX_CONFIG;

  // statSync( join( __dirname, configPath ) ).isFile();
  const config = require( configPath );

  if ( config ) {
    logger.trace( 'config: %o', config );

    if ( process.env.DMX_PORTPATH ) {
      config.output.device = process.env.DMX_PORTPATH;
    }

    initDMX(config);

    /*const testColor = new DMXColor( {
      rgba: '255,255,255,0.5'
    } );

    logger.trace( 'testColor: %O', testColor );
    logger.trace( 'r: %d', testColor.red );
    logger.trace( 'g: %d', testColor.green );
    logger.trace( 'b: %d', testColor.blue );
    logger.trace( 'a: %d', testColor.alpha );
    logger.trace( 'd: %d', testColor.dimmer );
    logger.trace( 'c: %d', testColor.cold );
    logger.trace( 'w: %d', testColor.warm ); */

  } else {
    logger.error( 'no config at configPath: %s', join( __dirname, configPath ) );
  }
}
