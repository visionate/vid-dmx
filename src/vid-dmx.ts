import { forIn, keys } from 'lodash';
import { DMXController, IDriver } from './com/dmx/DMX-Controller';
import { DMXDevice } from './com/dmx/DMX-Device';
import { DMXColor } from './com/dmx/model/DMXColor';
import { ColorCommand, DMXCommand, IDMXEvent } from './com/dmx/model/DMXEvent';
import { CLog, CLogger } from './com/visionate/logging/Logger';
import { IOClient } from './com/visionate/network/Socket.IO-Client';

export type Drivers = {
  [ key: string ]: {};
};

export type DMXDevices = {
  [ key: string ]: DMXDevice;
};

export type DeviceConfigs = {
  [ key: string ]: DeviceConfig;
};

export type DeviceConfig = {
  channels: string[]; groups: number | number[]; address: number;
};

export type FadePresetsCommand = {
  from?: string; to?: string; time: number; loop?: boolean;
};

export class DMXManager {
  public logger: CLogger = CLog.createClogger( 'DMXManager' );

  public static npmName = require( '../package.json' ).name;

  public static npmVersion = require( '../package.json' ).version;

  public dmxController: DMXController;

  private readonly drivers: Drivers = {};

  private devices: DMXDevices = {};

  private socketIOClient: IOClient;

  constructor( config: any ) {
    this.logger.info( '%s:%s', DMXManager.npmName, DMXManager.npmVersion );
    this.drivers = config.drivers;
    this.dmxController = new DMXController();
    this.socketIOClient = new IOClient().createClient( config.socketIOclient.proto, config.socketIOclient.host,
      config.socketIOclient.port, [ 'dmxEvent' ] );

    this.initDMX( config );
  }

  private initDMX( config: any ) {
    this.logger.trace( 'initDMX config: %o this.drivers: %o', config, this.drivers );

    const universe = this.dmxController.addUniverse( 'DMX', config.output.driver, config.output.device,
      this.drivers[ config.output.driver ] );
    this.initDevices( config, universe );
  }

  private initDevices( config: any, universe: IDriver ) {
    const devices: DeviceConfigs = config.devices;
    this.logger.trace( 'initDevices devices: %o', devices );
    forIn( devices, ( value: DeviceConfig, key ) => {
      this.devices[ key ] = new DMXDevice( key, value, universe );
    } );
    this.setupSOcketIO( config );
    this.runTestCues();
  }

  public setupSOcketIO( config: any ) {

    this.socketIOClient.connect();

    this.socketIOClient.on( 'dmxEvent', ( dmxEvent: (IDMXEvent | IDMXEvent[]) ) => {
      this.logger.trace( 'on dmxEvent: %O', dmxEvent );
      if ( Array.isArray( dmxEvent ) ) {
        dmxEvent.forEach( evt => {
          this.runDMXEvent( evt );
        } );
      } else {
        this.runDMXEvent( dmxEvent );
      }
    } );
  }

  protected runDMXEvent( dmxEvent: IDMXEvent ) {
    let commandSequence: DMXCommand[];

    const runCommandSequence = ( deviceID: string, data: DMXCommand ) => {
      // run command
      // this.logger.trace( 'runCommandSequence: %O', data );
      this.runCommand( deviceID, data );
      // run next command with fadeTime
      const next = commandSequence.splice( 0, 1 )[ 0 ];
      if ( next ) {
        next.delay = (data.delay || 0) + (data.fadeTime || 0);
        runCommandSequence( deviceID, next );
      }
    };

    const processCommand = ( deviceID: string, data: (DMXCommand | DMXCommand[]) ) => {
      if ( Array.isArray( data ) ) {
        commandSequence = data.map( ( cmd, index ) => {
          cmd.resetTweens = (index === 0) ? true : false;
          return cmd;
        } );
        runCommandSequence( deviceID, commandSequence.splice( 0, 1 )[ 0 ] );
      } else {
        // this.logger.trace( 'on dmxEvent run:%O', data );
        this.runCommand( deviceID, data );
      }
    };

    if ( Array.isArray( dmxEvent.deviceID ) ) {
      dmxEvent.deviceID.forEach( deviceID => {
        processCommand( deviceID, dmxEvent.data! );
      } );
    } else {
      processCommand( dmxEvent.deviceID as string, dmxEvent.data! );
    }
  }

  protected runCommand( deviceID: string, dmxCommand: DMXCommand ) {
    const device = this.devices[ deviceID ];
    if ( !device ) {
      this.logger.error( 'device not found: %s. Use one of: %o dmxCommand: %o', deviceID, keys( this.devices ).join( ', ' ),
        dmxCommand );
    } else {

      this.logger.info( 'dmxCommand: %o', dmxCommand );

      let commandData: ColorCommand;

      switch ( dmxCommand.command ) {
        case 'color':
          commandData = dmxCommand as ColorCommand;

          const fromColor = typeof commandData.fromColor !== 'undefined' ? new DMXColor( commandData.fromColor ) : null;
          const toColor = new DMXColor( commandData.toColor );

          device.setColor( fromColor, toColor, parseInt( commandData.delay as any ), parseInt( commandData.fadeTime as any ),
            parseInt( commandData.repeat as any ), commandData.swing, commandData.resetTweens, commandData.universeIndex );
          break;
      }
    }
  };

  public runTestCues(): void {
    this.logger.info( 'runTestCues' );

    /*  const commandData = {
     deviceID: 'DHL',
     'data': [
     {
     command: 'color',
     toColor: { 'rgba': '255,0,0,1' },
     fadeTime: 1,
     }, {
     command: 'color',
     toColor: { 'rgba': '255,255,255,1' },
     fadeTime: 3000,
     }, {
     command: 'color',
     toColor: { 'celvin': 6000 },
     fadeTime: 3000,
     },
     ],
     };*/

    const commandData = {
      deviceID: 'DHL',
      'data': [
        {
          command: 'color',
          toColor: { 'rgba': '255,255,255,1' },
          fadeTime: 1,
        },
      ],
    };

    this.runDMXEvent( commandData as any );

    /*  const morgenMittag: ColorCommand = {
     command: 'color',
     fromColor: { 'celvin': 3000 },
     toColor: { 'celvin': 6000 },
     fadeTime: 6000,
     };
     const mittagAbends: ColorCommand = {
     command: 'color',
     fromColor: { 'celvin': 6000 },
     toColor: { 'celvin': 2700 },
     fadeTime: 6000,
     };*/

    // morgen 3000
    // mittags 6000
    // abends 2700
    /* this.runCommand( 'Lichtwute', morgenMittag );
     setTimeout( () => {
     this.runCommand( 'Lichtwute', mittagAbends );
     }, 6000 );*/
    /*
     const RFIDTABLESAN: ColorCommand = {
     command: 'color',
     toColor: {
     hexColor: '#FFFFFF',
     alpha: 1,
     },
     fadeTime: 3000,
     };

     const RFIDATABLESAUS: ColorCommand = {
     command: 'color',
     toColor: {
     hexColor: '#FFFFFF',
     alpha: 0,
     },
     fadeTime: 3000,
     };

     this.runCommand( 'RFIDTABLES', RFIDTABLESAN );
     setTimeout( () => {
     this.runCommand( 'RFIDTABLES', RFIDATABLESAUS );
     }, 6000 );*/
    /**
     this.devices[ 'RFID' ].setColor( new DMXColor( {
      hexColor: '#00ff00',
    } ), null, 4000, -1, true );
     */
  }

  public shutdown(): Promise<void> {
    this.logger.info( 'shutdown process.pid: %s', process.pid );
    if ( this.dmxController ) {
      return this.dmxController.disconnect();
    } else {
      return Promise.resolve();
    }
  }
}
